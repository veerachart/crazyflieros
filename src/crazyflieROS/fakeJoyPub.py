#!/usr/bin/env python

import rospy
import curses

from sensor_msgs.msg import Joy as JoyMSG

class FakeJoyNode:
    def __init__(self):
        #publisher for fake joy signal
        self.joy_pub = rospy.Publisher('joy', JoyMSG)
        self.joy_msg = JoyMSG()
        #self.joy_msg.axes = [0,0,0,0,0,0,0,0,0,0,-1.0,0,0,0,0,0,0,0,0,0]
        self.joy_msg.axes = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        #self.joy_msg.buttons = [0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0]
        self.joy_msg.buttons = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        
    def run_node(self):
        self.joy_pub.publish(self.joy_msg)
        
if __name__ == '__main__':
    rospy.init_node('fakeJoyPublisher')
    
    node = FakeJoyNode()
    r = rospy.Rate(30)
    stdscr = curses.initscr()
    curses.cbreak()
    curses.noecho()
    stdscr.keypad(1)

    stdscr.addstr(0,10,"Hit 'h' to toggle L1 button")
    stdscr.refresh()
    stdscr.nodelay(1)
    
    stdscr.addstr(5,10,"L1 Released")
    stdscr.refresh()
    
    key = ''
    while not rospy.is_shutdown():
        key = stdscr.getch()
        stdscr.refresh()
        if key == '\x03':
            break
        elif key == ord('h'):
            node.joy_msg.axes[10] = -1.0 - node.joy_msg.axes[10]
            node.joy_msg.buttons[10] = 1 - node.joy_msg.buttons[10]
            if node.joy_msg.buttons[10] == 1:
                stdscr.addstr(5,10,"L1 Pressed ")
            else:
                stdscr.addstr(5,10,"L1 Released")
            stdscr.refresh()
        node.run_node()
        r.sleep()
    curses.endwin()
