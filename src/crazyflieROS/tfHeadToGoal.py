#!/usr/bin/env python

import rospy
import tf
import math
import roslib
roslib.load_manifest("crazyflieROS")
from crazyflieROS.msg import state as CFState

if __name__ == '__main__':
    rospy.init_node('tf_head_to_goal')
    
    listener = tf.TransformListener()
    broadcaster = tf.TransformBroadcaster()
    publisher = rospy.Publisher("/state_head", CFState)
    state_head = CFState()
    
    rate = rospy.Rate(30)
    while not rospy.is_shutdown():
        try:
            tf_list = listener.getFrameStrings()
            for x in tf_list:
                if x.startswith('/head_'):
                    (trans,rot) = listener.lookupTransform('/world', x, rospy.Time(0))
                    matrix = tf.transformations.quaternion_matrix(rot)
                    z = matrix[0:2,2]       # Extract z axis
                    yaw = math.atan2(z[1],z[0])      # yaw = angle of z axis from /world's x axis
                    state_head.header.stamp = rospy.Time.now()
                    state_head.x = trans[0]
                    state_head.y = trans[1]
                    state_head.z = trans[2]
                    state_head.yaw = math.degrees(yaw)
                    publisher.publish(state_head)
                    trans2= (trans[0] - 1.5*math.cos(yaw), trans[1] - 1.5*math.sin(yaw), trans[2] + 0.6)
                    
                    broadcaster.sendTransform(trans2,
                                              tf.transformations.quaternion_from_euler(0,0,yaw),
                                              rospy.Time.now(),
                                              '/goal_head',
                                              '/world')
                    break
            
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
        
        rate.sleep()
