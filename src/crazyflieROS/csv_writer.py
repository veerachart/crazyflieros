#!/usr/bin/env python
import rospy
import roslib;
roslib.load_manifest("crazyflieROS")

from crazyflieROS.msg import state as CFState
from crazyflieROS.msg import cmd as CMD
import csv
import time
import sys,os
pathname = os.path.dirname(os.path.dirname(os.path.dirname(sys.argv[0])))
global start_time
start_time = None

class CSVWriter:
    def __init__(self,topic_name='state_cf'):
        global start_time
        self.sub_state = rospy.Subscriber(topic_name, CFState, self.msgReceived)
        if not os.path.isdir(pathname+"/logdata"):
            os.makedirs(pathname+"/logdata")
        if not os.path.isdir(pathname+"/logdata/"+time.strftime("%Y%m%d")):
            os.makedirs(pathname+"/logdata/"+time.strftime("%Y%m%d"))
        self.file_name = pathname+"/logdata/"+time.strftime("%Y%m%d")+"/"+time.strftime("%Y%m%d-%H%M")+"_"+topic_name+".csv"
        start_time = None
        self.f = open(self.file_name, 'wb')
        self.writer = csv.writer(self.f)
        self.writer.writerow("time,x,y,z,yaw".split(','))
            
    def msgReceived(self, msg):
        global start_time
        t = msg.header.stamp.to_time()
        if start_time == None:
            start_time = t
        try:
            data = '%.9f,%f,%f,%f,%f' % (t-start_time, msg.x, msg.y, msg.z, msg.yaw)
            self.writer.writerow(data.split(','))
        except csv.Error as e:
            sys.exit('file %s, line %d: %s' % (file_name, writer.line_num, e))
            
class CSVWriterCommand:
    def __init__(self,topic_name='cfjoy'):
        global start_time
        self.sub_state = rospy.Subscriber(topic_name, CMD, self.msgReceived)
        if not os.path.isdir(pathname+"/logdata"):
            os.makedirs(pathname+"/logdata")
        if not os.path.isdir(pathname+"/logdata/"+time.strftime("%Y%m%d")):
            os.makedirs(pathname+"/logdata/"+time.strftime("%Y%m%d"))
        self.file_name = pathname+"/logdata/"+time.strftime("%Y%m%d")+"/"+time.strftime("%Y%m%d-%H%M")+"_"+topic_name+".csv"
        start_time = None
        self.f = open(self.file_name, 'wb')
        self.writer = csv.writer(self.f)
        self.writer.writerow("time,roll,pitch,yaw,thrust,hover".split(','))
            
    def msgReceived(self, msg):
        global start_time
        t = msg.header.stamp.to_time()
        if start_time == None:
            start_time = t
        try:
            data = '%.9f,%f,%f,%f,%d,%s' % (t-start_time, msg.roll, msg.pitch, msg.yaw, msg.thrust,msg.hover)
            self.writer.writerow(data.split(','))
        except csv.Error as e:
            sys.exit('file %s, line %d: %s' % (file_name, writer.line_num, e))


    
    
if __name__ == '__main__':
    rospy.init_node('csv_writer')
    state_goal_writer = CSVWriter('state_goal')
    state_goal_f_writer = CSVWriter('state_goal_f')
    state_cf_writer = CSVWriter('state_cf')
    state_cferr_writer = CSVWriter('state_cferr')
    state_head_writer = CSVWriter('state_head')
    cfjoy_writer = CSVWriterCommand('cfjoy')
    rospy.spin()
